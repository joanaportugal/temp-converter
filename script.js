const celsiusToFahrenheit = (celsius) => celsius * 1.8 + 32;

const celsiusToKelvin = (celsius) => celsius + 273.15;

const fahrenheitToCelsius = (fahrenheit) => (fahrenheit - 32) / 1.8;

const fahrenheitToKelvin = (fahrenheit) =>
  celsiusToKelvin(fahrenheitToCelsius(fahrenheit));

const kelvinToCelsius = (kelvin) => kelvin - 273.15;

const kelvinToFahrenheit = (kelvin) =>
  celsiusToFahrenheit(kelvinToCelsius(kelvin));

const input_celsius = document.getElementById("input_celsius");
const input_fahrenheit = document.getElementById("input_fahrenheit");
const input_kelvin = document.getElementById("input_kelvin");

input_celsius.addEventListener("input", () => {
  if (input_celsius.value != "") {
    input_fahrenheit.value = celsiusToFahrenheit(
      parseFloat(input_celsius.value)
    ).toFixed(2);
    input_kelvin.value = celsiusToKelvin(
      parseFloat(input_celsius.value)
    ).toFixed(2);
  } else {
    input_fahrenheit.value = "";
    input_kelvin.value = "";
  }
});

input_fahrenheit.addEventListener("input", () => {
  if (input_fahrenheit.value != "") {
    input_celsius.value = fahrenheitToCelsius(
      parseFloat(input_fahrenheit.value)
    ).toFixed(2);
    input_kelvin.value = fahrenheitToKelvin(
      parseFloat(input_fahrenheit.value)
    ).toFixed(2);
  } else {
    input_celsius.value = "";
    input_kelvin.value = "";
  }
});

input_kelvin.addEventListener("input", () => {
  if (input_kelvin.value != "") {
    input_celsius.value = kelvinToCelsius(
      parseFloat(input_kelvin.value)
    ).toFixed(2);
    input_fahrenheit.value = kelvinToFahrenheit(
      parseFloat(input_kelvin.value)
    ).toFixed(2);
  } else {
    input_celsius.value = "";
    input_fahrenheit.value = "";
  }
});
